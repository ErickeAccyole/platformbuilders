# README #

Para iniciar a aplicação é necessario a criação do banco de dados com o nome "platformbuilders";

O banco de dados usado na aplicação é postgres. No arquivo application.properties tem as opções de rodar
	localmente ou numa instancia docker(Principal).

O arquivo PlatformBuilders.postman_collection.json contém todas as requisiçoes necessárias para utilizar a aplicação.

A aplicação também disponibiliza a documentação da api usando o Swagger.

Documentação API: http://localhost:8080/swagger-ui.html#/

Link da aplicação: http://localhost:8080/clientes
