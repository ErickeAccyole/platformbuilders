package br.com.platformbuilders.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import br.com.platformbuilders.entity.Cliente;
import br.com.platformbuilders.repository.ClienteRepository;

@Component
public class ClienteModel {
	private final ClienteRepository clienteRepository;
	
	private static void validaCliente(Cliente cliente) throws Exception {
		if (cliente.getCpf() == null || "".equals(cliente.getCpf())) {
			throw new Exception("O campo cpf não pode ser vazio!");
		}
		
		if (cliente.getNome() == null || "".equals(cliente.getNome())) {
			throw new Exception("O campo nome não pode ser vazio!");
		}
		
		if (cliente.getDataNascimento() == null || "".equals(cliente.getDataNascimento())) {
			throw new Exception("O campo data de nascimento nome não pode ser vazio!");
		}
	}
	
	private int calculaIdade(Date dataNascimento) {

		Calendar cn = Calendar.getInstance();
		cn.setTime(dataNascimento);

		Date dataAtual = new Date(System.currentTimeMillis());
		Calendar ca = Calendar.getInstance();
		ca.setTime(dataAtual);

		int idade = ca.get(Calendar.YEAR) - cn.get(Calendar.YEAR);
		if (ca.get(Calendar.MONTH) < cn.get(Calendar.MONTH)) {
			idade--;
		} else if (ca.get(Calendar.MONTH) == cn.get(Calendar.MONTH)) {
			if (ca.get(Calendar.DAY_OF_MONTH) < cn.get(Calendar.DAY_OF_MONTH))
				idade--;
		}
		return idade;
	}

	@Autowired
	public ClienteModel(ClienteRepository clienteRepository) {
		this.clienteRepository = clienteRepository;
	}
	
	public Cliente salvar(Cliente cliente) throws Exception {
		validaCliente(cliente);
		
		Cliente clienteConsulta = getByCpf(cliente.getCpf());

		if (clienteConsulta != null) {
			throw new Exception("Erro ao cadastrar, já existe um cliente com esse cpf!");
		}
		
		clienteRepository.save(cliente);
		cliente.setIdade(calculaIdade(cliente.getDataNascimento()));
		
		return cliente;
		
	}

	public void remove(long clienteId) throws Exception {
		Cliente cliente = getById(clienteId);

		if (cliente == null) {
			throw new Exception("Erro ao remover, cliente não encontrado!");
		}

		clienteRepository.deleteById(clienteId);
	}

	public Cliente update(Cliente cliente) throws Exception {
		validaCliente(cliente);
		
		Cliente clienteConsulta = getByCpf(cliente.getCpf());

		if (clienteConsulta != null && clienteConsulta.getId() != cliente.getId()) {
			throw new Exception("Erro ao atualizar, já existe um cliente com esse cpf!");
		}

		return clienteRepository.save(cliente);
	}
	
	public Cliente updateDataNascimento(String cpf, String dataNascimento) throws Exception {
		Cliente cliente = getByCpf(cpf);

		if (cliente == null) {
			throw new Exception("Erro ao atualizar a data de nascimento, não foi encontrado um cliente com esse cpf!");
		}
		
		SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy"); 
		Date data = formato.parse(dataNascimento);
		cliente.setDataNascimento(data);
		
		validaCliente(cliente);
		
		clienteRepository.save(cliente);
		cliente.setIdade(calculaIdade(cliente.getDataNascimento()));
		
		return cliente;
	}

	public Page<Cliente> getAll(Pageable pageable) throws Exception {
		Page<Cliente> clientes = clienteRepository.findAll(pageable); 
		for (Cliente cliente : clientes) {
			cliente.setIdade(calculaIdade(cliente.getDataNascimento()));
		}
		return clientes;
	}

	public Cliente getById(long id) throws Exception {
		Cliente cliente = clienteRepository.findById(id).get(); 
		
		if (cliente != null) {
			cliente.setIdade(calculaIdade(cliente.getDataNascimento()));
		}
		
		return cliente;
	}

	public Cliente getByCpf(String cpf) throws Exception {
		Cliente cliente = clienteRepository.findByCpf(cpf);
				
		if (cliente != null) {
			cliente.setIdade(calculaIdade(cliente.getDataNascimento()));
		}
		
		return cliente;
	}

	public Cliente getByCpfAndNome(String cpf, String nome) throws Exception {
		Cliente cliente = clienteRepository.findByCpfAndNome(cpf, nome);
		
		if (cliente == null) {
			throw new Exception("Cliente não encontrado!");
		} else {
			cliente.setIdade(calculaIdade(cliente.getDataNascimento()));
		}
		
		return cliente;
	}
}
