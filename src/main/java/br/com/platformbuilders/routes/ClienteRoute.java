package br.com.platformbuilders.routes;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.TextNode;

import br.com.platformbuilders.entity.Cliente;
import br.com.platformbuilders.model.ClienteModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api
@RequestMapping
@RestController
public class ClienteRoute {
	private ClienteModel clienteModel;
	
	@Autowired
	public ClienteRoute(ClienteModel clienteModel) {
		this.clienteModel = clienteModel;
	}
	
	@ApiOperation(value = "Salva um cliente na base de dados")
	@PostMapping(path = "clientes")
	public ResponseEntity<?> save(@RequestBody Cliente cliente) throws Exception {
		try {
			return new ResponseEntity<>(clienteModel.salvar(cliente), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value = "Atualiza um cliente na base de dados")
	@PutMapping(path = "clientes")
	public ResponseEntity<?> update(@RequestBody Cliente cliente) throws Exception {
		try {
			return new ResponseEntity<>(clienteModel.update(cliente), HttpStatus.OK);			
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value = "Atualiza a data de nascimento do cliente na base de dados")
	@PatchMapping(path = "clientes/atualiza-data-nascimento")
	public ResponseEntity<?> updateDataNascimento(@RequestParam String cpf, @RequestBody Map<String, Object> dataNascimento) throws Exception {
		try {
			return new ResponseEntity<>(clienteModel.updateDataNascimento(cpf, dataNascimento.get("dataNascimento").toString()), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value = "Remove um cliente da base de dados")
	@DeleteMapping(path = "clientes/{id}")
	public ResponseEntity<?> remove(@PathVariable long id) throws Exception {
		try {
			clienteModel.remove(id);
			
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value = "Recupera os clientes na base de dados")
	@GetMapping(path = "clientes")
	public ResponseEntity<?> getAll(Pageable pageable) throws Exception {
		try {
			return new ResponseEntity<>(clienteModel.getAll(pageable), HttpStatus.OK);			
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value = "Consulta um cliente por cpf e nome na base de dados")
	@GetMapping(path="clientes/consulta-por-cpf-e-nome")
	public ResponseEntity<?> getByCpfAndNome(@RequestParam String cpf, @RequestParam String nome) throws Exception {
		try {
			return new ResponseEntity<>(clienteModel.getByCpfAndNome(cpf, nome), HttpStatus.OK);			
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
