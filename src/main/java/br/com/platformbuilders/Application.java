package br.com.platformbuilders;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.platformbuilders.config.CargaBanco;

@SpringBootApplication
public class Application {
	
private CargaBanco cargaBanco;
	
	@Autowired
	public Application(CargaBanco cargaBanco) {
		this.cargaBanco = cargaBanco;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@PostConstruct
    private void init() throws Exception {
        System.out.println("Iniciando carga de banco!");
        cargaBanco.carregaClientes();
        System.out.println("Fim da carga do banco!");
    }
}
