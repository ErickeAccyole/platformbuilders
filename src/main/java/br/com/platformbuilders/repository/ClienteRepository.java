package br.com.platformbuilders.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.platformbuilders.entity.Cliente;

public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Long>{
	
	Cliente findByCpf(String cpf);
	
	Cliente findByNome(String nome);
	
	Cliente findByCpfAndNome(String cpf, String nome);
}
