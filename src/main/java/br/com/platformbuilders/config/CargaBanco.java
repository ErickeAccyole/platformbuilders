package br.com.platformbuilders.config;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import br.com.platformbuilders.entity.Cliente;
import br.com.platformbuilders.model.ClienteModel;

@Component
public class CargaBanco {
	private final ClienteModel clienteModel;

	public CargaBanco(ClienteModel clienteModel) {
		this.clienteModel = clienteModel;
	}
	
	public void carregaClientes() throws Exception {
		int i = 0;
		while (i < 10) {
			String cpf = "0891111111" + i;
			Cliente clienteConsulta = clienteModel.getByCpf(cpf);
			
			if (clienteConsulta == null) {
				Cliente cliente = new Cliente();
				cliente.setCpf(cpf);
				cliente.setNome("Cliente" + i);
				
				SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy"); 
				Date data = formato.parse("01-01-199" + i);
				
				cliente.setDataNascimento(data);
				
				clienteModel.salvar(cliente);				
			}
			
			i++;
		}
	}
}
